using System.Drawing;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using SmartPark.Annotations;
using SmartPark.Windows;
using Point = System.Drawing.Point;

namespace SmartPark.Models
{
    public class ParkingPlace : INotifyPropertyChanged
    {
        private string _label;
        public string Label
        {
            get { return _label; }
            set
            {
                _label = value;
                ParkingPlacePropertyChanged(nameof(Label));
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                ParkingPlacePropertyChanged(nameof(Description));
            }
        }

        public Rectangle Roi { get; set; }
        public Point[] ContourPoints { get; set; }
        public int UiThinkness { get; set; } = 1;
        public double LEdge { get; set; } = MainWindow.LEdge;
        
        private Mat _cameraMat;
        [XmlIgnore]
        public Mat CameraMat
        {
            get { return _cameraMat; }
            set { _cameraMat = new Mat(value, Roi); }
        }

        [XmlIgnore]
        public Mat MaskMat { get; set; }
        
        private Mat _emptyMat;
        [XmlIgnore]
        public Mat EmptyMat
        {
            get { return _emptyMat; }
            set { _emptyMat = new Mat(value, Roi); }
        }

        [XmlIgnore]
        public double LMetric { get; set; }
        
        private bool _occupied;
        [XmlIgnore]
        public bool Occupied
        {
            get { return _occupied; }
            set
            {
                _occupied = value;
                ParkingPlacePropertyChanged(nameof(Occupied));
            }
        }

        public MCvScalar Color => LMetric > LEdge ? new MCvScalar(0, 0, 255) : new MCvScalar(0, 255, 0);

        public ParkingPlace() { }

        public ParkingPlace(Rectangle roi, Mat mask, Point[] points, string label, string description)
        {
            Roi = roi;
            ContourPoints = points;
            MaskMat = mask;
            EmptyMat = MainWindow.EmptyParkMat;
            _label = label;
            _description = description;
        }

        public void CalculateLMetric()
        {
            // LMetric = CvInvoke.Norm(EmptyMat, CameraMat, NormType.L1, MaskMat);
            // LMetric = CvInvoke.Norm(EmptyMat, CameraMat, NormType.L1, MaskMat) / CvInvoke.CountNonZero(MaskMat);
            LMetric = CvInvoke.Norm(CameraMat, EmptyMat, NormType.RelativeL1, MaskMat);
            Occupied = LMetric > LEdge;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void ParkingPlacePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}