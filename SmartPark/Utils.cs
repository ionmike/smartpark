﻿using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Point = System.Drawing.Point;

namespace SmartPark
{
    internal static class Utils
    {
        public static BitmapSource CreateBitmapSource(IImage frame)
        {
            using (var frameStream = new MemoryStream())
            {
                frameStream.Seek(0, SeekOrigin.Begin);
                frame.Bitmap.Save(frameStream, System.Drawing.Imaging.ImageFormat.Bmp);

                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = new MemoryStream(frameStream.ToArray());
                bitmap.EndInit();
                bitmap.Freeze();

                return bitmap;
            }
        }

        public static Mat CreateMaskFromPoints(Point[] points, out int x, out int y, out int width, out int height)
        {
            int minX = points.Min(p => p.X);
            int maxX = points.Max(p => p.X);
            int minY = points.Min(p => p.Y);
            int maxY = points.Max(p => p.Y);

            x = minX;
            y = minY;
            width = maxX - minX;
            height = maxY - minY;

            Mat blackMat = new Mat(maxY, maxX, DepthType.Cv8U, 1);
            blackMat.SetTo(new MCvScalar(0));
            CvInvoke.FillConvexPoly(blackMat, new VectorOfPoint(points.ToArray()), new MCvScalar(255));

            Mat resultMask = new Mat(blackMat, new Rectangle(x, y, width, height));

            return resultMask;
        }

        public static Mat CreateMaskFromPoints(Point[] points)
        {
            int minX = points.Min(p => p.X);
            int maxX = points.Max(p => p.X);
            int minY = points.Min(p => p.Y);
            int maxY = points.Max(p => p.Y);

            Mat blackMat = new Mat(maxY, maxX, DepthType.Cv8U, 1);
            blackMat.SetTo(new MCvScalar(0));
            CvInvoke.FillConvexPoly(blackMat, new VectorOfPoint(points.ToArray()), new MCvScalar(255));

            Mat resultMask = new Mat(blackMat, new Rectangle(minX, minY, maxX-minX, maxY-minY));

            return resultMask;
        }
    }
}
