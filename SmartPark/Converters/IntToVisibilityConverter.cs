﻿using System;
using System.Windows.Data;

namespace SmartPark.Converters
{
    class IntToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targettype, object parameter, System.Globalization.CultureInfo culture)
        {
            return (int)value > 0 ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
