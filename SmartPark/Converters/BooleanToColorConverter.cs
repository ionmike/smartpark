using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace SmartPark.Converters
{
    public class BooleanToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            SolidColorBrush retColor;
            if ((bool)value)
            {
                retColor = Application.Current.TryFindResource("RedBrush") as SolidColorBrush;
            }
            else
            {
                retColor = Application.Current.TryFindResource("GreenBrush") as SolidColorBrush;
            }
            return retColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Equals(value, Application.Current.TryFindResource("GreenBrush") as SolidColorBrush);
        }
    }
}