using System;
using System.Windows;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using SmartPark.Models;
using SmartPark.Windows;

namespace SmartPark.Controls
{
    public partial class ParkingPlaceCard
    {
        private ParkingPlace _parkingPlace;
        private Action _action;

        public ParkingPlaceCard()
        {
            InitializeComponent();
        }

        public ParkingPlaceCard(ParkingPlace parkingPlace, Action action)
        {
            _parkingPlace = parkingPlace;
            _action = action;
            InitializeComponent();
            DataContext = _parkingPlace;
        }

        private void DeleteParkZone_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            _action();
            _parkingPlace = null;
            ((Panel) Parent).Children.Remove(this);
        }

        private void EditParkZone_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            var dialog = new DescriptionDialogBox(_parkingPlace) {Owner = Application.Current.MainWindow };

            if (dialog.ShowDialog() != true) return;
            _parkingPlace.Label = dialog.LabelText;
            _parkingPlace.Description = dialog.DescriptionText;
        }

        private void ViewParkZone_Click(object sender, RoutedEventArgs e)
        {
            _parkingPlace.UiThinkness = 2;
            new Task(delegate {
                Thread.Sleep(5000);
                Dispatcher.Invoke(delegate
                {
                    if (_parkingPlace != null) _parkingPlace.UiThinkness = 1;
                });
            }).Start();
        }
    }
}