﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Animation;
using SmartPark.Annotations;

namespace SmartPark.Controls
{
    public partial class InfoPanel : INotifyPropertyChanged
    {
        public bool IsShows;
        private string _text;
        public string Text {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                InfoPanelPropertyChanged(nameof(Text));
            }
        }

        private Storyboard _animation;

        public InfoPanel()
        {
            InitializeComponent();
            DataContext = this;
        }

        public void Show()
        {
            _animation = (Storyboard) TryFindResource("ShowPanel");
            _animation.Completed += delegate { IsShows = true; };
            _animation.Begin();
        }

        public void Hide()
        {
            _animation = (Storyboard)TryFindResource("HidePanel");
            _animation.Completed += delegate { IsShows = false; };
            _animation.Begin();
        }

        public void ShowShort()
        {
            _animation = (Storyboard)TryFindResource("ShowPanelShort");
            IsShows = true;
            _animation.Completed += delegate { IsShows = false; };
            _animation.Begin();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void InfoPanelPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
