﻿namespace SmartPark.Windows
{
    public partial class AboutWindow
    {
        public AboutWindow()
        {
            InitializeComponent();

            MouseLeftButtonDown += delegate { Close(); };
        }
    }
}
