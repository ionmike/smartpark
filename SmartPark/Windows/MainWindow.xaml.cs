using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Microsoft.Win32;
using SmartPark.Models;
using SmartPark.Controls;
using SmartPark.Annotations;
using Point = System.Drawing.Point;
using Rectangle = System.Drawing.Rectangle;

namespace SmartPark.Windows
{
    public partial class MainWindow : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void MainWindowPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private Capture _captureDevice;

        private DispatcherTimer _imageTimer = new DispatcherTimer();

        private Mat _cameraFrame;
        private Mat _grayMat = new Mat();

        private bool _captureInProgress = true;

        private bool _isEditorMode;

        public bool IsEditorMode
        {
            get { return _isEditorMode; }
            set
            {
                _isEditorMode = value;
                MainWindowPropertyChanged(nameof(IsEditorMode));
            }
        }

        private bool _imageViewerShowParkingPlaces = true;

        private InfoPanel _infoPanel = new InfoPanel();

        private List<Point> _clickPoints = new List<Point>();

        private ObservableCollection<ParkingPlace> _parkingPlaces;

        public ObservableCollection<ParkingPlace> ParkingPlaces
        {
            get { return _parkingPlaces; }
            set
            {
                _parkingPlaces = value;
                MainWindowPropertyChanged();
            }
        }

        public static Mat EmptyParkMat;
        public static double LEdge = 0.35;

        public MainWindow()
        {
            InitializeComponent();

            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeWindow, OnCanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeWindow, OnCanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreWindow, OnCanResizeWindow));

            ViewerGrid.Children.Add(_infoPanel);

            ParkingPlaces = new ObservableCollection<ParkingPlace>();

            CvInvoke.UseOpenCL = false;
            try
            {
                _captureDevice = new Capture();
                _captureDevice.SetCaptureProperty(CapProp.FrameWidth, 1280);
                _captureDevice.SetCaptureProperty(CapProp.FrameHeight, 720);
                _imageTimer.Tick += RenderImageTick;
                _imageTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);

                try
                {
                    EmptyParkMat = new Mat("empty_park.jpg", LoadImageType.Grayscale);
                }
                catch (ArgumentException)
                {
                    EmptyParkMat = _captureDevice.QueryFrame();
                    CvInvoke.CvtColor(EmptyParkMat, EmptyParkMat, ColorConversion.Bgr2Gray);
                    EmptyParkMat.Save("empty_park.jpg");
                }

                _imageTimer.Start();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
            
            DataContext = this;
        }

        #region UIControls
        private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
        {
            SaveParkingLot("saved_parking_lot.xml");

            SystemCommands.CloseWindow(this);
        }

        private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
            Restore.Visibility = Visibility.Visible;
            Maximize.Visibility = Visibility.Collapsed;
        }

        private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
            Maximize.Visibility = Visibility.Visible;
            Restore.Visibility = Visibility.Collapsed;
        }
        #endregion

        private void RenderImageTick(object sender, EventArgs e)
        {
            _cameraFrame = _captureDevice.QueryFrame();
            if (_cameraFrame == null) return;

            CvInvoke.CvtColor(_cameraFrame, _grayMat, ColorConversion.Bgr2Gray);

            foreach (var parkingPlace in ParkingPlaces)
            {
                parkingPlace.CameraMat = _grayMat;
                parkingPlace.CalculateLMetric();
                if (_imageViewerShowParkingPlaces)
                {
                    CvInvoke.Polylines(_cameraFrame, parkingPlace.ContourPoints, true, parkingPlace.Color, parkingPlace.UiThinkness);
                }
            }
            if (IsEditorMode && _clickPoints.Count >= 2)
            {
                CvInvoke.Polylines(_cameraFrame, _clickPoints.ToArray(), false, new MCvScalar(0, 255, 0));
            }

            ImageViewer.Source = Utils.CreateBitmapSource(_cameraFrame);
        }

        private void ImageViewer_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            double scaleX = ImageViewer.Source.Width / ImageViewer.ActualWidth;
            double scaleY = ImageViewer.Source.Height / ImageViewer.ActualHeight;

            int x = (int)(e.GetPosition(ImageViewer).X * scaleX);
            int y = (int)(e.GetPosition(ImageViewer).Y * scaleY);

            _clickPoints.Add(new Point(x, y));
            if (_clickPoints.Count <= 3) return;

            var dialog = new DescriptionDialogBox { Owner = this };
            if (dialog.ShowDialog() == true)
            {
                int rectX, rectY, rectWidth, rectHeight;
                Mat mask = Utils.CreateMaskFromPoints(_clickPoints.ToArray(), out rectX, out rectY, out rectWidth, out rectHeight);

                // TODO Labels must be unique
                ParkingPlace parkingPlace = new ParkingPlace(new Rectangle(rectX, rectY, rectWidth, rectHeight), mask, _clickPoints.ToArray(), dialog.LabelText, dialog.DescriptionText);
                ParkingPlaces.Add(parkingPlace);

                ParkZonesStackPanel.Children.Add(new ParkingPlaceCard(parkingPlace, () =>
                {
                    ParkingPlaces.Remove(parkingPlace);
                }));
            }
            _clickPoints.Clear();
        }

        #region ViewerButtons
        private void CaptureButton_Click(object sender, RoutedEventArgs e)
        {
            if (_captureDevice == null) return;
            if (_captureInProgress)
            {
                _infoPanel.Text = "Захват остановлен";
                _infoPanel.ShowShort();

                CaptureToggleButton.Content = "Возобновить захват";

                _captureDevice.Pause();
                _imageTimer.Stop();
            }
            else
            {
                _infoPanel.Text = "Захват возобновлен";
                _infoPanel.ShowShort();

                CaptureToggleButton.Content = "Остановить захват";

                _captureDevice.Start();
                _imageTimer.Start();
            }
            
            _captureInProgress = !_captureInProgress;
        }

        private void ToggleEditorModeButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsEditorMode)
            {
                _infoPanel.Hide();

                EditorModeToggleButton.Content = "Добавить места парковок";
                ImageViewer.Cursor = Cursors.Arrow;

                ImageViewer.MouseLeftButtonDown -= ImageViewer_MouseLeftButtonDown;
            }
            else
            {
                _infoPanel.Text = "Включен режим редактора\nВыделите место четырьмя точками";
                _infoPanel.Show();

                EditorModeToggleButton.Content = "Выйти из режима редактора";
                ImageViewer.Cursor = Cursors.Cross;

                ImageViewer.MouseLeftButtonDown += ImageViewer_MouseLeftButtonDown;
            }
            _clickPoints.Clear();

            IsEditorMode = !IsEditorMode;
        }

        private void ShowParkingPlacesToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (_imageViewerShowParkingPlaces)
            {
                _infoPanel.Text = "Показ разметки выключен";
                _infoPanel.ShowShort();

                ShowParkingPlacesToggleButton.Content = "Показывать разметку";
            }
            else
            {
                _infoPanel.Text = "Показ разметки включен";
                _infoPanel.ShowShort();

                ShowParkingPlacesToggleButton.Content = "Скрыть раметку";
            }

            _imageViewerShowParkingPlaces = !_imageViewerShowParkingPlaces;
        }
        #endregion

        #region MainMenu
        private void EmptyParkOpenFromFileMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog
            {
                DefaultExt = ".jpg",
                Filter = "jpg files(*.jpg)|*.jpg|All files (*.*)|*.*",
                CheckPathExists = true
            };
            bool? result = openFile.ShowDialog();

            if (result != true) return;

            EmptyParkMat = new Mat(openFile.FileName, LoadImageType.Grayscale);
            foreach (var parkingPlace in ParkingPlaces)
            {
                parkingPlace.EmptyMat = EmptyParkMat;
            }

            _infoPanel.Text = "Пустая парковка обновлена";
            _infoPanel.ShowShort();
        }

        private void EmptyParkShotFromCameraMenuItem_Click(object sender, RoutedEventArgs e)
        {
            _captureDevice.Pause();
            _imageTimer.Stop();

            EmptyParkMat = _captureDevice.QueryFrame();
            CvInvoke.CvtColor(EmptyParkMat, EmptyParkMat, ColorConversion.Bgr2Gray);

            foreach (var parkingPlace in ParkingPlaces)
            {
                parkingPlace.EmptyMat = EmptyParkMat;
            }
            EmptyParkMat.Save("empty_park_from_camera.jpg");

            _captureDevice.Start();
            _imageTimer.Start();

            _infoPanel.Text = "Снимок сделан и был сохранен";
            _infoPanel.ShowShort();
        }

        private void SettingsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow(LEdge) { Owner = this };

            if (settingsWindow.ShowDialog() == true)
            {
                LEdge = settingsWindow.LEdge;
                foreach (var parkingPlace in ParkingPlaces)
                {
                    parkingPlace.LEdge = LEdge;
                }

                _infoPanel.Text = "Значение границы обновлено";
                _infoPanel.ShowShort();
            }
        }

        private void SaveParkMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                FileName = "data",
                DefaultExt = ".xml",
                Filter = "xml files(*.xml)|*.xml|All files (*.*)|*.*",
                CheckPathExists = true
            };
            bool? result = saveFileDialog.ShowDialog();
            if (result != true) return;

            SaveParkingLot(saveFileDialog.FileName);

            _infoPanel.Text = "Разметка парковки была сохранена";
            _infoPanel.ShowShort();
        }
        
        // TODO check its original park savefile
        private void LoadParkMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                DefaultExt = ".xml",
                Filter = "xml files(*.xml)|*.xml|All files (*.*)|*.*",
                CheckPathExists = true
            };
            bool? result = openFileDialog.ShowDialog();

            if (result != true) return;
            ParkingPlaces.Clear();

            LoadParkingLot(openFileDialog.FileName);

            _infoPanel.Text = "Разметка загружена";
            _infoPanel.ShowShort();
        }

        private void AboutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            new AboutWindow { Owner = this }.ShowDialog();
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        #endregion

        private void SaveParkingLot(string fileName)
        {
            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<ParkingPlace>));
            using (StreamWriter wr = new StreamWriter(fileName))
            {
                xs.Serialize(wr, ParkingPlaces);
            }
        }

        private void LoadParkingLot(string fileName)
        {
            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<ParkingPlace>));
            using (StreamReader rd = new StreamReader(fileName))
            {
                ParkingPlaces = (ObservableCollection<ParkingPlace>) xs.Deserialize(rd);
            }

            if (ParkingPlaces == null) return;

            ParkZonesStackPanel.Children.Clear();

            foreach (var parkingPlace in ParkingPlaces)
            {
                parkingPlace.EmptyMat = EmptyParkMat;
                parkingPlace.MaskMat = Utils.CreateMaskFromPoints(parkingPlace.ContourPoints);

                ParkZonesStackPanel.Children.Add(new ParkingPlaceCard(parkingPlace, () =>
                {
                    ParkingPlaces.Remove(parkingPlace);
                }));
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try { 
                LoadParkingLot("saved_parking_lot.xml");
            }
            catch (FileNotFoundException)
            {
                _infoPanel.Text = "Не найден последней сохраненной файл разметки";
                _infoPanel.ShowShort();
            }
        }

        #region DragAndDropInStackPanel
        private bool _isDragging;
        private System.Windows.Point _startPoint;
        private int _oldIndex, _newIndex;
        private UIElement _realDragSource = new UIElement();
        private UIElement _dummyDragSource = new UIElement();

        private void ParkZonesStackPanel_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _startPoint = e.GetPosition(ParkZonesStackPanel);
        }

        private void ParkZonesStackPanel_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _isDragging = false;

            _realDragSource.ReleaseMouseCapture();
        }

        private void ParkZonesStackPanel_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed) return;

            System.Windows.Point mousePos = e.GetPosition(ParkZonesStackPanel);
            Vector diff = _startPoint - mousePos;

            if (_isDragging ||
                (!(Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance) &&
                 !(Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))) return;

            _isDragging = true;

            _realDragSource = e.Source as UIElement;
            _realDragSource?.CaptureMouse();

            _oldIndex = ParkZonesStackPanel.Children.IndexOf(_realDragSource);

            DragDrop.DoDragDrop(_dummyDragSource, new DataObject("UIElement", e.Source, true), DragDropEffects.Move);
        }

        private void ParkZonesStackPanel_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("UIElement")) return;

            e.Effects = DragDropEffects.Move;
        }
        
        private void ParkZonesStackPanel_Drop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("UIElement")) return;

            UIElement droptarget = e.Source as UIElement;

            _newIndex = ParkZonesStackPanel.Children.IndexOf(droptarget);

            if (_newIndex != -1)
            {
                ParkZonesStackPanel.Children.Remove(_realDragSource);
                ParkZonesStackPanel.Children.Insert(_newIndex, _realDragSource);
            }

            ParkingPlaces.Move(_oldIndex, _newIndex);

            _isDragging = false;
            _realDragSource.ReleaseMouseCapture();
        }
        #endregion
    }
}
