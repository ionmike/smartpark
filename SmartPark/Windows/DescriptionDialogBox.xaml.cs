using SmartPark.Models;

namespace SmartPark.Windows
{
    public partial class DescriptionDialogBox
    {
        public DescriptionDialogBox()
        {
            InitializeComponent();
        }

        public DescriptionDialogBox(ParkingPlace parkingPlace)
        {
            InitializeComponent();
            DataContext = parkingPlace;
        }

        public string LabelText
        {
            get { return LabelTextBox.Text; }
            set { LabelTextBox.Text = value; }
        }

        public string DescriptionText
        {
            get { return DescriptionTextBox.Text; }
            set { DescriptionTextBox.Text = value; }
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
