﻿using System.Windows;

namespace SmartPark.Windows
{
    public partial class SettingsWindow
    {
        public SettingsWindow()
        {
            InitializeComponent();
        }

        public SettingsWindow(double lEdge)
        {
            InitializeComponent();
            LEdge = lEdge;
            DataContext = this;
        }

        public double LEdge { get; set; }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
